function flip() {
  return Math.random() >= 0.5;
};

function randomNumber(n) {
  var n = parseInt(document.getElementById("number").value, 10);
  var result = 0;
  if (n == 1){
    result = 0;
    document.getElementById("random").innerHTML = result;
  }

  else{
    if (n <= 1000000 && n >= 1){
      for(i = 0; i < n; i++){
          result = result + flip();
      }
      document.getElementById("random").innerHTML = result;
    }
    else{
      document.getElementById("random").innerHTML = 'Number must be grater than 0 and less than 1,000,000'
    }
  }
};
